const express = require("express");
const bcrypt = require("bcrypt");
// const fs = require("fs");

// const saltRounds = 10; // the number of salt rounds to use
// const hashedPassword = bcrypt.hashSync("password1", salt);

const app = express();

// parse the request body as JSON
app.use(express.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); // allow requests from any domain
  res.header(
  "Access-Control-Allow-Headers",
  "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
  });

// a hard-coded list of users for the sake of simplicity
// const users = [
//   {
//     username: 'user1',
//     password: 'password1'
//   },
//   {
//     username: 'user2',
//     password: 'password2'
//   }
// ];
const fs = require("fs");

const users = JSON.parse(fs.readFileSync("users.json"));

app.post("/signup", (req, res) => {
  const { username, password } = req.body;

  const saltRounds = 10; // number of salt rounds to use for hashing
  const salt = bcrypt.genSaltSync(saltRounds); // generate a salt
  const hash = bcrypt.hashSync(password, salt); // hash the password with the salt

  // create a new user object with the hashed password
  const newUser = {
    username: username,
    password: hash,
  };

  // add the new user to the list of users
  users.push(newUser);

  // update the users.json file with the new list of users
  fs.writeFileSync("users.json", JSON.stringify(users));

  // send a success status
  res.sendStatus(200);
});

// a route to handle the login request
app.post("/login", (req, res) => {
  // get the username and password from the request
  const { username, password } = req.body;

  // find the user with the matching username
  const user = users.find((u) => u.username === username);

  if (user) {
    // if the user is found, compare the hashed password with the provided password
    if (bcrypt.compareSync(password, user.password)) {
      // if the passwords match, send a 200 OK response
      res.sendStatus(200);
    } else {
      // if the passwords don't match, send a 401 Unauthorized response
      res.sendStatus(401);
    }
  } else {
    // if the user is not found, send a 404 Not Found response
    res.sendStatus(404);
  }
});

// start the server
const port = 2500;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

// const fs = require('fs');
// const bcrypt = require('bcrypt');

// const users = [
//   {
//     username: 'mike',
//     password: 'owinoo'
//   },
//   {
//     username: 'user2',
//     password: 'password2'
//   }
// ];

// // hash and salt the passwords
// const hashedUsers = users.map(user => {
//   return {
//     ...user,
//     password: bcrypt.hashSync(user.password, 10)
//   };
// });

// // write the hashed users to the JSON file
// fs.writeFileSync('users.json', JSON.stringify(hashedUsers));
